Uses TasksForEmployee;
var
time: data;

BEGIN
EnterData(time);
writeln('The number of weeks: ',QuantityWeek(time));
writeln('Tasks which required more than 8 hours to complete: ');
OutputTasksExceedEightHours(time);
END.
