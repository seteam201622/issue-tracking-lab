unit TasksForEmployee;

interface

type data = array of integer;

function EnterQuantity(): integer;
procedure EnterData(var time:data);
function DisplaysError(timestring:string): boolean;
function QuantityWeek(var time:data): integer;
procedure OutputTasksExceedEightHours(var time:data);
function TransformationTime(line: string): integer;

implementation

function EnterQuantity():integer;
var numbers: integer;
Begin
   readln(numbers);
   EnterQuantity := numbers;
end;

function DisplaysError(timestring:string):boolean;
Begin
  if ((Length(timestring) = 5)and (timestring[3] = ':') and (timestring[1] >= '0')
  and (timestring[1] <= '9') and (timestring[2] >= '0') and (timestring[2] <= '9')
  and (timestring[4] >= '0')  and (timestring[4] <= '9') and (timestring[5] >= '0')
  and (timestring[5] <= '9'))then DisplaysError := true
  else DisplaysError := false;
end;

procedure EnterData(var time:data);
var i: integer;
    timestring: string;
Begin
   write('Enter the number of tasks: ');
   SetLength(time,EnterQuantity);
   writeln('Enter the time:');
   i := 0;
   while i < length(time) do
   begin
      write(i + 1, ' task: ');
      readln(timestring);
      if (DisplaysError(timestring)) then
         time[i] := TransformationTime(timestring)
      else
      begin
        writeln('Error! Re-enter: ');
        i -= 1;
      end;
      i += 1;
   end;
end;

function QuantityWeek(var time:data):integer;
var
AllMinuts, AllWeeks, i: integer;
Begin
   AllMinuts := 0;
   i := 0;
   while i < length(time) do
   begin
      AllMinuts += time[i];
      i += 1;
   end;
   AllWeeks := Round(AllMinuts / 60 / 40);
   QuantityWeek := AllWeeks;
end;

procedure OutputTasksExceedEightHours(var time:data);
var i: integer;
Begin
   i := 0;
   while i < length(time) do
   begin
      if (time[i] > 480) then
         writeln(i + 1, ' task: ', (time[i] / 60):0:2);
      i += 1;
   end;
end;

function TransformationTime(line: string): integer;
var
  StrHours, StrMinute: string;
  number1, number2, error: integer;
Begin
  StrHours := copy(line, 1, 2);
  val(StrHours, number1, error);
  StrMinute := copy(line, 4, 2);
  val(StrMinute, number2, error);
  TransformationTime := number1 * 60 + number2;
end;

end.
